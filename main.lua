--[[ LOVE STUFF ]]
function love.load()
  maxTime = 0
  timer = 0
  score = 0

  gameState = "menu"

  sprites = {}
  sprites.player = love.graphics.newImage('assets/character.png')
  sprites.bullet = love.graphics.newImage('assets/bullet.png')
  sprites.zombie = love.graphics.newImage('assets/zombie.png')
  sprites.background = love.graphics.newImage('assets/background.jpg')

  player = {}
  player.speed = 180
  player.sprite = sprites.player

  zombies = {}
  bullets = {}

  killableTables = {bullets, zombies}
end

function love.update(dt)
  if gameState == "game" then
    movePlayer(dt)
    moveZombie(dt)
    moveBullet(dt)

    checkCollisions(dt)
    cleanUp(dt)
    timer = timer - dt
    if timer <= 0 then
      spawnZombie()
      maxTime = maxTime * 0.95
      timer = maxTime
    end
  end
  if gameState == "menu" then
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(love.graphics.newFont(40))
    love.graphics.printf("Click anywhere to begin",  0, 50, love.graphics.getWidth(), "center")
  end
end

function love.draw()
  if gameState == "game" then
    love.graphics.draw(sprites.background, 0, 0)
    drawPlayer()
    drawZombies()
    drawBullets()
    love.graphics.setFont(love.graphics.newFont(20))
    love.graphics.print("Score:" .. score)
  end
end

function love.mousepressed(x, y, button, isTouch)
  if button == 1 then
    if gameState == "game" then
      spawnBullet()
    end
    if gameState == "menu" then
      newGame()
    end
  end
end

--[[ GENERAL ]]
function newGame()
  score = 0
  player.x = love.graphics.getWidth()/2
  player.y = love.graphics.getHeight()/2
  clearZombies()
  gameState = "game"
  maxTime = 2
  timer = maxTime
end


function checkCollisions(dt)
  -- zombies and player
  for i,z in ipairs(zombies) do
    if distanceBetweenObjects(z, player) < 30 then
      gameState = "menu" -- gameover
    end
  end
  -- zombies and bullets
  for i,z in ipairs(zombies) do
    for j,b in ipairs(bullets) do
      if distanceBetweenObjects(z, b) < 30 then
        z.dead = true
        b.dead = true
        score = score + 1
      end
    end
  end
end


function cleanUp(dt)
  for x, t in ipairs(killableTables) do
    for i=#t, 1, -1 do
      if t[i].dead == true then
          table.remove(t, i)
      end
    end
  end
end


--[[ PLAYER ]]
function drawPlayer()
  love.graphics.draw(
    player.sprite,
    player.x, player.y,
    player.rotation,
    nil, nil,
    15.5, 21.5
  )
end

function movePlayer(dt)
  player.rotation = radians_between({["x"] = love.mouse.getX(), ["y"] = love.mouse.getY()}, player)

  if love.keyboard.isDown('s') then
    player.y = player.y + player.speed * dt
  end
  if love.keyboard.isDown('w') then
    player.y = player.y - player.speed * dt
  end

  if love.keyboard.isDown('a') then
    player.x = player.x - player.speed * dt
  end
  if love.keyboard.isDown('d') then
    player.x = player.x + player.speed * dt
  end
  player.x = math.Clamp(0, player.x, love.graphics.getWidth())
  player.y = math.Clamp(0, player.y, love.graphics.getHeight())
end

--[[ ZOMBIE STUFF ]]
function drawZombies()
  for i,z in ipairs(zombies) do
    love.graphics.draw(
      z.sprite,
      z.x, z.y,
      radians_between(player, z),
      nil, nil,
      z.sprite:getWidth()/2, z.sprite:getHeight()/2
    )
  end
end
function spawnZombie()
  zombie = {}
  zombie.x = math.random(0, love.graphics.getWidth())
  zombie.y = math.random(0, love.graphics.getHeight())
  zombie.dead = false
  zombie.speed = 100
  zombie.sprite = sprites.zombie

  local side = math.random(1, 4)
  if side == 1 then
    zombie.x = -30
  elseif side == 2 then
    zombie.y = -30
  elseif side == 3 then
    zombie.x = love.graphics.getWidth() + 30
  elseif side == 4 then
    zombie.y = love.graphics.getHeight() + 30
  end

  table.insert(zombies, zombie)
end

function moveZombie(dt)
  for i,z in ipairs(zombies) do
    radians = radians_between(player, z)
    z.x = z.x + math.cos(radians) * z.speed * dt
    z.y = z.y + math.sin(radians) * z.speed * dt
  end
end

function clearZombies()
  for i=#zombies, 1, -1 do
    table.remove(zombies, i)
  end
end
--[[ BULLET ]]
function spawnBullet()
  bullet = {}
  bullet.x = player.x
  bullet.y = player.y
  bullet.dead = false
  bullet.speed = 500
  bullet.sprite = sprites.bullet
  bullet.direction = player.rotation

  table.insert(bullets, bullet)
end

function drawBullets()
  for i, b in ipairs(bullets) do
    love.graphics.draw(
      b.sprite,
      b.x, b.y,
      b.direction,
      0.5, 0.5,
      b.sprite:getWidth()/2, b.sprite:getHeight()/2
    )
    -- 46, 31
  end
end

function moveBullet(dt)
  for i, b in ipairs(bullets) do
    b.x = b.x + math.cos(b.direction) * b.speed * dt
    b.y = b.y + math.sin(b.direction) * b.speed * dt
  end

  for i=#bullets, 1, -1 do
    local b = bullets[i]
    if b.x < 0 or b.x > love.graphics.getWidth() or b.y < 0 or b.y > love.graphics.getHeight() then
      table.remove(bullets, i)
    end
  end
end

--[[ MATH STUFF ]]

function distanceBetweenObjects(obj1, obj2)
  return distanceBetween(obj1.x, obj1.y, obj2.x, obj2.y)
end

function distanceBetween(x1, y1, x2, y2)
  return math.sqrt((y2 - y1)^2 + (x2 - x1)^2)
end

function radians_between(me, target)
  return math.atan2(me.y - target.y, me.x - target.x)
end


function math.Clamp(lower, val, upper)
    assert(val and lower and upper, "not very useful error message here")
    if lower > upper then lower, upper = upper, lower end -- swap if boundaries supplied the wrong way
    return math.max(lower, math.min(upper, val))
end
